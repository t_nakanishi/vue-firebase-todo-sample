import Vue from 'vue'
import App from './App.vue'
import store from './store'
//import firebaseApp from './firebaseConfigured'

new Vue({
  el: '#app',
  store,
  template : '<App />',
  components: { App }
})
