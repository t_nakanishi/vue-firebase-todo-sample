import firebaseApp from 'firebase/app'
import 'firebase/firestore'

firebaseApp.initializeApp({
  apiKey: "AIzaSyCl3fAvLE2hrmtoGj8tek11G4HoW8Gbd30",
  authDomain: "myfirstfirebase-75a4e.firebaseapp.com",
  databaseURL: "https://myfirstfirebase-75a4e.firebaseio.com",
  projectId: "myfirstfirebase-75a4e",
  storageBucket: "",
  messagingSenderId: "622762610284"
})

export default firebaseApp
