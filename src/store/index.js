import * as types from './mutation-types'
import Vue from 'vue'
import Vuex from 'vuex'

import firebaseApp from '../firebaseConfigured'

let db = firebaseApp.firestore()
db.settings({ timestampsInSnapshots:true})

let todosRef = db.collection("todos");

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    items: []
  },
  actions: {
    [types.INIT_TASK] ({commit}){
      todosRef.get().then(function(querySnapshot) {
        let items = [];
        querySnapshot.forEach(function(doc) {
          let item = doc.data()
          item.id = doc.id
          items.push(item)
        })
        commit(types.INIT_TASK, items)
      })
    },
    [types.ADD_TASK] ({ commit }, title) {
      let newItem = {
        title: title,
        is_do: false
      }
      todosRef.add(newItem).then(function(docRef) {
        newItem.id = docRef.id;
        commit( types.ADD_TASK, {data: newItem})        
      });
    },
    [types.DONE_TASK] ({ commit }, item) {
      todosRef.doc(item.id).update({is_do : !item.is_do})
      commit( types.DONE_TASK, { data: item })
    }
  },
  mutations: {
    [types.ADD_TASK] (state, payload) {
      state.items.push(payload.data);
    },
    [types.DONE_TASK] (state, payload) {
      let index = state.items.indexOf(payload.data)
      state.items[index].is_do = !payload.data.is_do
    },
    [types.INIT_TASK] (state, payload) {
      for(let key in payload){
        state.items.push(payload[key]) 
      }
    }
  },
  plugins:[ 
    (store) => { store.dispatch(types.INIT_TASK) }
  ]
});
